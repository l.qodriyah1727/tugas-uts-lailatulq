from django.contrib import admin
from django.urls import path

from . import views
from akun import views as akunViews
from beranda import views as berandaViews
from history import views as historyViews

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('akun/', akunViews.index),
    path('beranda/', berandaViews.index),
    path('history/', historyViews.index)
]
